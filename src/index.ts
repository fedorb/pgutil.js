import * as fs from "fs";
import * as path from "path";
import * as spawn from "child_process";
import * as util from "util";

const execAsync = util.promisify(spawn.exec);
const writeFileAsync = util.promisify(fs.writeFile);
const unlinkAsync = util.promisify(fs.unlink);

export async function restoreAsync(config: { database: string, backup: string }) {
    let backupFullName = path.resolve(config.backup);
    let listFullName = path.resolve(path.dirname(backupFullName), `.${path.basename(backupFullName)}.list`);

    let cmd = `pg_restore -l "${backupFullName}"`;
    console.log(cmd);
    let { stdout } = await execAsync(cmd);
    let lines = stdout.split(/\r?\n/);
    lines = lines.filter(e => !e.match(/TABLE DATA .+? seller_catalog_node_vendor_join /));
    await writeFileAsync(listFullName, lines.join("\r\n"));

    cmd = `pg_restore -a --disable-triggers -e -j 8 -L "${listFullName}" -d ${config.database} "${backupFullName}"`;
    console.log(cmd);
    await execAsync(cmd);

    await unlinkAsync(listFullName);

    cmd = `pg_restore -a -e -j 8 -t seller_catalog_node_vendor_join -d ${config.database} "${backupFullName}"`;
    console.log(cmd);
    await execAsync(cmd);
}