import * as minimist from "minimist";
import * as index from "../index";

const args = minimist(process.argv.splice(2));
index.restoreAsync({ database: args.database, backup: args.backup });